#include <stdio.h>
//This programme will reverse the number

int main()
{
    int n,x=0;
    printf("Enter a number:\n");
    scanf("%d",&n);
    while(n!=0)
    {
        x=x*10;
        x=x+n%10;
        n=n/10;
    }
    printf("%d\n",x);
    return 0;
}