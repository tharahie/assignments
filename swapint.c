#include <stdio.h>
// This programme will swap two integers

float main()
{
    float x,y;
    printf("Enter two numbers:\n");
    scanf("%f",&x);
    scanf("%f",&y);
    printf("Before swapping\nx=%.2f y=%.2f",x,y);
    x=x*y;
    y=x/y;
    x=x/y;
    printf("After swapping\nx = %.2f y = %.2f",x,y);
}