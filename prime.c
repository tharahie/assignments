#include <stdio.h>
//This programme will determine if number is a prime number

int main()
{
    int n=0,i,flag=0;
    printf("Enter a number\n");
    scanf("%d",&n);
    for(i=2;i<=n/2;++i)
    {
        if(n%i==0)
        {
            flag=1;
            break;
        }
    }
    if(n==1)
        printf("1 is neither a prime number nor a composite number\n");
        else
        if(flag==0)
        printf("It is a prime number\n");
        else
        printf("It is not a prime number\n");
        return 0;
}