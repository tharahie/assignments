#include <stdio.h>
//This programme will calculate the sum of all positive integers ungtil we enter zero or a negative value

int main()
{
    int total=0, n=0;
    do
    {
        printf("Enter a positive number:\n");
        scanf("%d",&n);
        if(n<=0) break;
        total+=n;
    }
    while (n>0);
    printf("Total=%d\n",total);
    return 0;
}