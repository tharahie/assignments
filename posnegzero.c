#include <stdio.h>
//This programme will check whether a number is positive or negative or zero

float main()
{
    float x=0;
    printf("Enter the number:\n");
    scanf("%f",&x);
    if(x>0)
    printf("Number is positive\n");
    else if(x<0)
    printf("Number is negative\n");
    else
    printf("Number is zero\n");
    return 0;
}
